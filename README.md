# Instructions

After clone, be sure that docker.io and docker-compose are installed, then 
run the commands bellow in sequence from the project root directory:

``docker-compose up -d``\
``docker exec -it php bash``\
``cp .env.example .env``\
``composer install``\
``vendor/bin/phinx migrate``\
``vendor/bin/phinx seed:run``

**Obs:** *after run the commands above, the application will be available in 
the port 8080 of the local host. The phpMyAdmin too, but in the port 8081. 
To access the mysql by the phpMyAdmin GUI, the server name is `mysql`, user 
and password are both `root`.*

# Routes
`[GET] /prime-numbers`\
`[GET] /find-missing-char`\
`[GET] /find-next-tv-series` => **Query String Params**: (***title:*** 
`string`, 
***dateTime:***
`Iso 
8601 Date String`)\
`[GET] /get-random-promotional-design` => **Query String Params**:
(***promotionId:*** 
`integer`),