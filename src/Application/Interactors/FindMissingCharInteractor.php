<?php

namespace Jmslapa\ExadsTest\Application\Interactors;

use Jmslapa\ExadsTest\Domain\Entities\RandomAsciiArray;
use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharResponseModel;

class FindMissingCharInteractor implements FindMissingCharInputPort
{
    public function __construct(
        private readonly FindMissingCharOutputPort $outputPort
    ) {
    }

    public function findMissingChar(): ViewModel
    {
        $randomArray = new RandomAsciiArray();
        return $this->outputPort->missingCharFoundSuccessfully(
            new FindMissingCharResponseModel($randomArray)
        );
    }
}