<?php

namespace Jmslapa\ExadsTest\Application\Interactors;

use DateTime;
use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\ListTvSeries;
use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\ListTvSeriesFilterPayload;
use Jmslapa\ExadsTest\Application\Interfaces\ErrorLogger;
use Jmslapa\ExadsTest\Domain\Entities\TvSeries;
use Jmslapa\ExadsTest\Domain\Entities\TvSeriesInterval;
use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirRequestModel;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirResponseModel;

class FindNextTvSeriesThatWillAirInteractor implements
    FindNextTvSeriesThatWillAirInputPort
{
    public function __construct(
        private readonly FindNextTvSeriesThatWillAirOutputPort $outputPort,
        private readonly ListTvSeries $tvSeriesDataProvider,
        private readonly ErrorLogger $errorLogger
    ) {
    }

    public function findNextTvSeries(
        FindNextTvSeriesThatWillAirRequestModel $requestModel
    ): ViewModel {
        try {
            $dateParam = $requestModel->getDateTime() ?? new DateTime();
            $filters = new ListTvSeriesFilterPayload($requestModel->getTitle());
            $tvSeriesList = $this->tvSeriesDataProvider->listAll($filters);
            if (!count($tvSeriesList)) {
                return $this->outputPort->tvSeriesNotFound();
            }
            $intervals = array_reduce(
                $tvSeriesList, function ($acc, TvSeries $curr) {
                array_push($acc, ...$curr->getIntervals());
                return $acc;
            }, []
            );

            usort($intervals, fn(
                TvSeriesInterval $a,
                TvSeriesInterval $b
            ) => $a->getNextShowDateTime($dateParam)->getTimestamp(
                ) - $b->getNextShowDateTime($dateParam)->getTimestamp());

            $nextTvSeriesInterval = array_shift($intervals);

            return $this->outputPort->nextTvSeriesFound(
                new FindNextTvSeriesThatWillAirResponseModel(
                    $nextTvSeriesInterval->getTvSeries()
                )
            );
        } catch (\Throwable $e) {
            $this->errorLogger->report('FIND NEXT TV SERIES', $e);
            return $this->outputPort->unexpectedError();
        }
    }
}