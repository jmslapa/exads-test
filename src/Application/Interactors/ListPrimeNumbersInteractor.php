<?php

namespace Jmslapa\ExadsTest\Application\Interactors;

use Jmslapa\ExadsTest\Domain\Entities\Number;
use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersResponseModel;

class ListPrimeNumbersInteractor implements ListPrimeNumbersInputPort
{
    public function __construct(
        private readonly ListPrimeNumbersOutputPort $outputPort
    ) {
    }

    public function listPrimeNumbers(): ViewModel
    {
        $numbers = array_map(fn($n) => new Number($n), range(1, 100));

        return $this->outputPort->numbersListedSuccessfully(
            new ListPrimeNumbersResponseModel($numbers)
        );
    }

}