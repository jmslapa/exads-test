<?php

namespace Jmslapa\ExadsTest\Application\Interactors;

use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\FindPromotionById;
use Jmslapa\ExadsTest\Application\Interfaces\ErrorLogger;
use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignRequestModel;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignResponseModel;

class GetRandomPromotionalDesignInteractor implements
    GetRandomPromotionalDesignInputPort
{
    public function __construct(
        private readonly GetRandomPromotionalDesignOutputPort $outputPort,
        private readonly FindPromotionById $promotionRepository,
        private readonly ErrorLogger $errorLogger
    ) {
    }

    public function getRandomPromotionalDesign(
        GetRandomPromotionalDesignRequestModel $requestModel
    ): ViewModel {
        try {
            $promotion = $this->promotionRepository->findPromotionById(
                $requestModel->getPromotionId()
            );

            if (!$promotion) {
                return $this->outputPort->promotionNotFound();
            }

            if (!$promotion->getDesignsCount()) {
                return $this->outputPort->promotionalDesignsNotFound();
            }

            return $this->outputPort->promotionalDesignSuccessfullyRetrieved(
                new GetRandomPromotionalDesignResponseModel(
                    $promotion->getRandomDesign()
                )
            );
        } catch (\Throwable $e) {
            $this->errorLogger->report('GET PROMOTIONAL DESIGN', $e);
            return $this->outputPort->unexpectedError($e);
        }
    }

}