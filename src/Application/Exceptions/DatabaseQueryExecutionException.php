<?php

namespace Jmslapa\ExadsTest\Application\Exceptions;

use Exception;

class DatabaseQueryExecutionException extends Exception
{
    public function __construct(
        protected $message,
        private readonly string $query,
        private readonly array $params
    ) {
        parent::__construct($message);
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}