<?php

namespace Jmslapa\ExadsTest\Application\Interfaces\DataProviders;

use Jmslapa\ExadsTest\Domain\Entities\TvSeries;

interface ListTvSeries
{
    /**
     * @param ListTvSeriesFilterPayload $filterPayload
     * @return TvSeries[]
     */
    public function listAll(ListTvSeriesFilterPayload $filterPayload): array;
}