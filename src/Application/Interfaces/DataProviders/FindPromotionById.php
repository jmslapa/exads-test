<?php

namespace Jmslapa\ExadsTest\Application\Interfaces\DataProviders;

use Jmslapa\ExadsTest\Domain\Entities\Promotion;

interface FindPromotionById
{
    public function findPromotionById(int $id): ?Promotion;
}