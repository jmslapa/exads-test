<?php

namespace Jmslapa\ExadsTest\Application\Interfaces\DataProviders;

class ListTvSeriesFilterPayload
{
    public function __construct(
        private readonly ?string $title
    ) {
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
}