<?php

namespace Jmslapa\ExadsTest\Application\Interfaces;

use Jmslapa\ExadsTest\Application\Exceptions\DatabaseConnectionException;
use Jmslapa\ExadsTest\Application\Exceptions\DatabaseQueryExecutionException;

interface DatabaseFetch
{
    /**
     * @throws DatabaseConnectionException
     * @throws DatabaseQueryExecutionException
     */
    public function fetch(string $query, array $params = []): array;
}