<?php

namespace Jmslapa\ExadsTest\Application\Interfaces;

interface DatabaseConnection
{
    public function open(): void;

    public function close(): void;
}
