<?php

namespace Jmslapa\ExadsTest\Application\Interfaces;

interface ErrorLogger
{
    public function report(string $label, \Throwable $e): bool;
}