<?php

namespace Jmslapa\ExadsTest\Infra\Helpers;

use Jmslapa\ExadsTest\Application\Interfaces\ErrorLogger;

class ErrorLoggingHelper implements ErrorLogger
{
    private static ErrorLoggingHelper|null $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): ErrorLoggingHelper
    {
        if (!self::$instance) {
            self::$instance = new ErrorLoggingHelper();
        }

        return self::$instance;
    }

    public function report(string $label, \Throwable $e): bool
    {
        return error_log(
            "[{$label}]: {$e->getMessage()}\n{$e->getTraceAsString()}"
        );
    }

}