<?php

namespace Jmslapa\ExadsTest\Infra\Transformers;

use Jmslapa\ExadsTest\Domain\Entities\PromotionalDesign;
use Jmslapa\ExadsTest\Infra\Interfaces\PromotionalDesignTransformer;

class PromotionalDesignTransformerImpl implements PromotionalDesignTransformer
{
    private static PromotionalDesignTransformer|null $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): PromotionalDesignTransformer
    {
        if (!self::$instance) {
            self::$instance = new PromotionalDesignTransformerImpl();
        }

        return self::$instance;
    }

    public function fromResultSet(array $resultSet): PromotionalDesign
    {
        return new PromotionalDesign(
            designId: $resultSet['designId'],
            designName: $resultSet['designName'],
            splitPercent: $resultSet['splitPercent']
        );
    }

}