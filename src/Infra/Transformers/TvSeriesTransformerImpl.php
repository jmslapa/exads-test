<?php

namespace Jmslapa\ExadsTest\Infra\Transformers;

use Jmslapa\ExadsTest\Domain\Entities\TvSeries;
use Jmslapa\ExadsTest\Domain\Entities\TvSeriesInterval;
use Jmslapa\ExadsTest\Domain\Entities\WeekDay;
use Jmslapa\ExadsTest\Infra\Interfaces\TvSeriesTransformer;

class TvSeriesTransformerImpl implements TvSeriesTransformer
{
    private static ?TvSeriesTransformer $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): TvSeriesTransformer
    {
        if (!self::$instance) {
            self::$instance = new TvSeriesTransformerImpl();
        }

        return self::$instance;
    }

    public function fromResultSet(array $resultSet): array
    {
        $tvSeriesList = [];
        foreach ($resultSet as $row) {
            $tvSeries = array_filter(
                $tvSeriesList,
                fn(TvSeries $s) => $s->getId() === $row['id']
            )[0] ?? null;
            if (!$tvSeries) {
                $tvSeries = new TvSeries(
                    id: $row['id'],
                    title: $row['title'],
                    channel: $row['channel'],
                    gender: $row['gender'],
                    intervals: []
                );
                $tvSeriesList[] = $tvSeries;
            }

            $intervals = $tvSeries->getIntervals();
            $intervals[] = new TvSeriesInterval(
                idTvSeries: $tvSeries->getId(),
                weekDay: WeekDay::from($row['week_day']),
                showTime: $row['show_time'],
                tvSeries: $tvSeries
            );
            $tvSeries->setIntervals($intervals);
        }

        return $tvSeriesList;
    }
}