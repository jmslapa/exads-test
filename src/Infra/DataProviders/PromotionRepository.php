<?php

namespace Jmslapa\ExadsTest\Infra\DataProviders;

use Exads\ABTestData;
use Exads\ABTestException;
use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\FindPromotionById;
use Jmslapa\ExadsTest\Domain\Entities\Promotion;
use Jmslapa\ExadsTest\Infra\Interfaces\PromotionalDesignTransformer;

class PromotionRepository implements FindPromotionById
{
    public function __construct(
        private readonly PromotionalDesignTransformer $promotionalDesignTransformer
    ) {
    }

    public function findPromotionById(int $id): ?Promotion
    {
        try {
            $promotionData = new ABTestData($id);
            $designs = array_map(
                fn(array $rs
                ) => $this->promotionalDesignTransformer->fromResultSet($rs),
                $promotionData->getAllDesigns()
            );

            return new Promotion(
                id: $id,
                name: $promotionData->getPromotionName(),
                designs: $designs
            );
        } catch (ABTestException $e) {
            return null;
        }
    }
}