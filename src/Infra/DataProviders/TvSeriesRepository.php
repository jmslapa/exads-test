<?php

namespace Jmslapa\ExadsTest\Infra\DataProviders;

use Jmslapa\ExadsTest\Application\Exceptions\DatabaseConnectionException;
use Jmslapa\ExadsTest\Application\Exceptions\DatabaseQueryExecutionException;
use Jmslapa\ExadsTest\Application\Interfaces\DatabaseFetch;
use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\ListTvSeries;
use Jmslapa\ExadsTest\Application\Interfaces\DataProviders\ListTvSeriesFilterPayload;
use Jmslapa\ExadsTest\Infra\Interfaces\TvSeriesTransformer;

class TvSeriesRepository implements ListTvSeries
{
    public function __construct(
        private readonly DatabaseFetch $databaseConnection,
        private readonly TvSeriesTransformer $tvSeriesTransformer
    ) {
    }

    /**
     * @throws DatabaseConnectionException
     * @throws DatabaseQueryExecutionException
     */
    public function listAll(ListTvSeriesFilterPayload $filterPayload): array
    {
        $query = "
        select ts.id,
               ts.title,
               ts.channel,
               ts.gender,
               tsi.week_day,
               tsi.show_time
        from tv_series as ts
                 join tv_series_intervals tsi on ts.id = tsi.id_tv_series
        where (:title is null or ts.title like :title)
        ";

        $resultSet = $this->databaseConnection->fetch(
            $query,
            ['title' => "%{$filterPayload->getTitle()}%"]
        );

        return $this->tvSeriesTransformer->fromResultSet($resultSet);
    }
}