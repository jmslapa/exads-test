<?php

namespace Jmslapa\ExadsTest\Infra\Interfaces;

use Jmslapa\ExadsTest\Application\Interfaces\DatabaseConnection;
use Jmslapa\ExadsTest\Application\Interfaces\DatabaseFetch;

interface DatabaseConnectionContainer
{
    public function getConnection(string $driver
    ): DatabaseConnection & DatabaseFetch;
}