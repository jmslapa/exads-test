<?php

namespace Jmslapa\ExadsTest\Infra\Interfaces;

use Jmslapa\ExadsTest\Domain\Entities\TvSeries;

interface TvSeriesTransformer
{
    /**
     * @param array $resultSet
     * @return TvSeries[]
     */
    public function fromResultSet(array $resultSet): array;
}