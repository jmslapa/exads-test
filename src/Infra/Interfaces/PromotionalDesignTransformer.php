<?php

namespace Jmslapa\ExadsTest\Infra\Interfaces;

use Jmslapa\ExadsTest\Domain\Entities\PromotionalDesign;

interface PromotionalDesignTransformer
{
    public function fromResultSet(array $resultSet): PromotionalDesign;
}