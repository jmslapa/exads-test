<?php

namespace Jmslapa\ExadsTest\Infra\Database;

use Jmslapa\ExadsTest\Application\Exceptions\DatabaseConnectionException;
use Jmslapa\ExadsTest\Application\Exceptions\DatabaseQueryExecutionException;
use Jmslapa\ExadsTest\Application\Interfaces\DatabaseConnection;
use Jmslapa\ExadsTest\Application\Interfaces\DatabaseFetch;
use PDO;

class MysqlConnection implements DatabaseConnection, DatabaseFetch
{
    private ?PDO $connection = null;

    public function __construct(
        private readonly string $dbHost,
        private readonly string $dbPort,
        private readonly string $dbName,
        private readonly string $userName,
        private readonly string $password,
    ) {
    }

    public function open(): void
    {
        if (!$this->connection) {
            $this->connection = new PDO(
                dsn: "mysql:host={$this->dbHost};port={$this->dbPort};dbname={$this->dbName}",
                username: $this->userName,
                password: $this->password,
                options: [
                    PDO::ATTR_PERSISTENT => true
                ]
            );
        }
    }

    public function close(): void
    {
        if ($this->connection) {
            $this->connection = null;
        }
    }

    public function fetch(string $query, array $params = []): array
    {
        if (!$this->connection) {
            throw new DatabaseConnectionException(
                "The database connection must be opened before use."
            );
        }

        $st = $this->connection->prepare($query);
        if (!$st->execute($params)) {
            throw new DatabaseQueryExecutionException(
                "Query execution has failed.", $query, $params
            );
        }

        return $st->fetchAll(PDO::FETCH_ASSOC);
    }

}