<?php

namespace Jmslapa\ExadsTest\Infra\Database;

use Jmslapa\ExadsTest\Application\Interfaces\DatabaseConnection;
use Jmslapa\ExadsTest\Application\Interfaces\DatabaseFetch;
use Jmslapa\ExadsTest\Infra\Interfaces\DatabaseConnectionContainer;

class DatabaseConnectionComposite implements DatabaseConnection,
                                             DatabaseConnectionContainer
{
    private static ?DatabaseConnectionComposite $instance = null;

    private function __construct(
        private readonly array $connections
    ) {
    }

    public static function getInstance(): DatabaseConnectionComposite
    {
        if (!self::$instance) {
            self::$instance = new DatabaseConnectionComposite([
                'mysql' => new MysqlConnection(
                    dbHost: env('DB_HOST'),
                    dbPort: env('DB_PORT'),
                    dbName: env('DB_NAME'),
                    userName: env('DB_USER'),
                    password: env('DB_PASS')
                )
            ]);
        }

        return self::$instance;
    }

    public function open(): void
    {
        foreach ($this->connections as $connection) {
            $connection->open();
        }
    }

    public function close(): void
    {
        foreach ($this->connections as $connection) {
            $connection->close();
        }
    }

    public function getConnection(string $driver
    ): DatabaseConnection&DatabaseFetch {
        return $this->connections[$driver];
    }


}