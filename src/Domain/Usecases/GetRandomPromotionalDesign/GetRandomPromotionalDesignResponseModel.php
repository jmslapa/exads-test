<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign;

use Jmslapa\ExadsTest\Domain\Entities\PromotionalDesign;

class GetRandomPromotionalDesignResponseModel
{
    public function __construct(
        private readonly PromotionalDesign $promotion
    ) {
    }

    public function getPromotionalDesign(): PromotionalDesign
    {
        return $this->promotion;
    }
}