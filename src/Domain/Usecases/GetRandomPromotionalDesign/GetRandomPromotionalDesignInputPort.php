<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface GetRandomPromotionalDesignInputPort
{
    public function getRandomPromotionalDesign(
        GetRandomPromotionalDesignRequestModel $requestModel
    ): ViewModel;
}