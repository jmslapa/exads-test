<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface GetRandomPromotionalDesignOutputPort
{
    public function promotionalDesignSuccessfullyRetrieved(
        GetRandomPromotionalDesignResponseModel $responseModel
    ): ViewModel;

    public function promotionNotFound(): ViewModel;

    public function promotionalDesignsNotFound(): ViewModel;

    public function unexpectedError(\Throwable $e): ViewModel;
}