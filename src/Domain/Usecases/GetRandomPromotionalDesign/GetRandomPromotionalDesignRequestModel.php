<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign;

class GetRandomPromotionalDesignRequestModel
{
    public function __construct(
        private readonly int $promotionId
    ) {
    }

    public function getPromotionId(): int
    {
        return $this->promotionId;
    }
}