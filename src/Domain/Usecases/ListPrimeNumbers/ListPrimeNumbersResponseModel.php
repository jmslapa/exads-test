<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers;

use Jmslapa\ExadsTest\Domain\Entities\Number;

class ListPrimeNumbersResponseModel
{
    /**
     * @param Number[] $numbers
     */
    public function __construct(
        private readonly array $numbers
    ) {
    }

    public function getModel(): array
    {
        $model = [];
        foreach ($this->numbers as $number) {
            $model[$number->getValue()] = !$number->isPrime() ? implode(
                ',',
                $number->getMultiples()
            ) : 'PRIME';
        }
        return $model;
    }


}