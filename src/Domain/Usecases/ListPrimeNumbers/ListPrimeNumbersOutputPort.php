<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface ListPrimeNumbersOutputPort
{
    public function numbersListedSuccessfully(
        ListPrimeNumbersResponseModel $responseModel
    ): ViewModel;
}