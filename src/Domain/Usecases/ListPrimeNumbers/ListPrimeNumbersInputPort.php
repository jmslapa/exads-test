<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface ListPrimeNumbersInputPort
{
    public function listPrimeNumbers(): ViewModel;
}