<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir;

use DateTime;

class FindNextTvSeriesThatWillAirRequestModel
{
    public function __construct(
        private readonly ?string $title,
        private readonly ?DateTime $dateTime
    ) {
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return DateTime|null
     */
    public function getDateTime(): ?DateTime
    {
        return $this->dateTime;
    }
}
