<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface FindNextTvSeriesThatWillAirInputPort
{
    public function findNextTvSeries(
        FindNextTvSeriesThatWillAirRequestModel $requestModel
    ): ViewModel;
}