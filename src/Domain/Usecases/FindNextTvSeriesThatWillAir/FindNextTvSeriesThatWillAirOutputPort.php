<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface FindNextTvSeriesThatWillAirOutputPort
{
    public function nextTvSeriesFound(
        FindNextTvSeriesThatWillAirResponseModel $responseModel
    ): ViewModel;

    public function tvSeriesNotFound(): ViewModel;

    public function unexpectedError(): ViewModel;
}