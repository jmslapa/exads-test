<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir;

use Jmslapa\ExadsTest\Domain\Entities\TvSeries;

class FindNextTvSeriesThatWillAirResponseModel
{
    public function __construct(
        private readonly TvSeries $tvSeries
    ) {
    }

    /**
     * @return TvSeries
     */
    public function getTvSeries(): TvSeries
    {
        return $this->tvSeries;
    }


}