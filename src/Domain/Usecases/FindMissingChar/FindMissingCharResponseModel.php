<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar;

use Jmslapa\ExadsTest\Domain\Entities\RandomAsciiArray;

class FindMissingCharResponseModel
{
    public function __construct(
        private readonly RandomAsciiArray $array,
    ) {
    }

    public function getModel(): array
    {
        return [
            'array' => implode(' ', $this->array->getArray()),
            'missingChar' => $this->array->getMissingChar()
        ];
    }
}
