<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface FindMissingCharInputPort
{
    public function findMissingChar(): ViewModel;
}