<?php

namespace Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;

interface FindMissingCharOutputPort
{
    public function missingCharFoundSuccessfully(
        FindMissingCharResponseModel $responseModel
    ): ViewModel;
}