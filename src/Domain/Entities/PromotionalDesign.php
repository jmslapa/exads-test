<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

class PromotionalDesign
{
    public function __construct(
        private int $designId,
        private string $designName,
        private int $splitPercent,
    ) {
    }

    public function getDesignId(): int
    {
        return $this->designId;
    }

    public function setDesignId(int $designId): void
    {
        $this->designId = $designId;
    }

    public function getDesignName(): string
    {
        return $this->designName;
    }

    public function setDesignName(string $designName): void
    {
        $this->designName = $designName;
    }

    public function getSplitPercent(): int
    {
        return $this->splitPercent;
    }

    public function setSplitPercent(int $splitPercent): void
    {
        $this->splitPercent = $splitPercent;
    }
}