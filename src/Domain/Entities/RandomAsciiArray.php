<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

class RandomAsciiArray
{
    private readonly array $array;

    public function __construct()
    {
        $this->array = $this->generateRandomArray();
    }

    protected function generateRandomArray(): array
    {
        $array = array_map(fn($n) => chr($n), range(44, 124));
        shuffle($array);
        $key = array_rand($array);
        unset($array[$key]);

        return $array;
    }

    public function getArray(): array
    {
        return $this->array;
    }

    public function getMissingChar(): string
    {
        $expectedSum = array_reduce(
            range(44, 124),
            fn($current, $total) => $total += $current,
            0
        );
        $currentSum = array_reduce(
            $this->array,
            fn($total, $current) => $total += ord($current),
            0
        );

        return chr($expectedSum - $currentSum);
    }
}