<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

class Promotion
{
    /**
     * @param int $id
     * @param string $name
     * @param PromotionalDesign[] $designs
     */
    public function __construct(
        private int $id,
        private string $name,
        private array $designs
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return PromotionalDesign[]
     */
    public function getDesigns(): array
    {
        return $this->designs;
    }

    /**
     * @param PromotionalDesign[] $designs
     * @return void
     */
    public function setDesigns(array $designs): void
    {
        $this->designs = $designs;
    }

    public function getDesignsCount(): int
    {
        return count($this->getDesigns());
    }

    public function getRandomDesign(): ?PromotionalDesign
    {
        $totalChance = array_reduce(
            array: $this->getDesigns(),
            callback: fn($acc, $cur) => $acc += $cur->getSplitPercent(),
            initial: 0
        );

        $limit = rand(0, $totalChance);
        $currentChance = 0;

        foreach ($this->getDesigns() as $design) {
            $currentChance += $design->getSplitPercent();
            if ($currentChance >= $limit) {
                return $design;
            }
        }

        return null;
    }
}