<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

use DateTime;

class TvSeriesInterval
{
    public function __construct(
        private string $idTvSeries,
        private WeekDay $weekDay,
        private string $showTime,
        private TvSeries $tvSeries
    ) {
    }

    /**
     * @return string
     */
    public function getIdTvSeries(): string
    {
        return $this->idTvSeries;
    }

    /**
     * @param string $idTvSeries
     */
    public function setIdTvSeries(string $idTvSeries): void
    {
        $this->idTvSeries = $idTvSeries;
    }

    /**
     * @return WeekDay
     */
    public function getWeekDay(): WeekDay
    {
        return $this->weekDay;
    }

    /**
     * @param WeekDay $weekDay
     */
    public function setWeekDay(WeekDay $weekDay): void
    {
        $this->weekDay = $weekDay;
    }

    /**
     * @return string
     */
    public function getShowTime(): string
    {
        return $this->showTime;
    }

    /**
     * @param string $showTime
     */
    public function setShowTime(string $showTime): void
    {
        $this->showTime = $showTime;
    }

    /**
     * @return TvSeries
     */
    public function getTvSeries(): TvSeries
    {
        return $this->tvSeries;
    }

    /**
     * @param TvSeries $tvSeries
     */
    public function setTvSeries(TvSeries $tvSeries): void
    {
        $this->tvSeries = $tvSeries;
    }


    public function getNextShowDateTime(DateTime $reference): DateTime
    {
        $reference = clone $reference;
        $referenceYear = (int)$reference->format('o');
        $referenceWeek = (int)$reference->format('W');
        $referenceDayOfWeek = (int)$reference->format('N');
        $seriesDayOfWeek = $this->weekDay->isoNumber();
        [$seriesHour, $seriesMinute, $seriesSecond] = explode(
            ':',
            $this->showTime
        );

        $timeFormat = 'H:i:s';
        $referenceTime = DateTime::createFromFormat(
            $timeFormat,
            $reference->format($timeFormat)
        );
        $seriesTime = DateTime::createFromFormat(
            $timeFormat,
            $this->getShowTime()
        );

        if ($seriesDayOfWeek < $referenceDayOfWeek ||
            ($seriesDayOfWeek === $referenceDayOfWeek && $referenceTime > $seriesTime)) {
            $referenceWeek++;
        }

        $reference->setISODate(
            year: $referenceYear,
            week: $referenceWeek,
            dayOfWeek: $seriesDayOfWeek
        )
            ->setTime(
                hour: $seriesHour,
                minute: $seriesMinute,
                second: $seriesSecond,
                microsecond: 0
            );

        return $reference;
    }
}
