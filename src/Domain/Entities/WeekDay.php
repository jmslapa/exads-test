<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

enum WeekDay: string
{
    case SUNDAY = 'SUNDAY';
    case MONDAY = 'MONDAY';
    case TUESDAY = 'TUESDAY';
    case WEDNESDAY = 'WEDNESDAY';
    case THURSDAY = 'THURSDAY';
    case FRIDAY = 'FRIDAY';
    case SATURDAY = 'SATURDAY';

    public function isoNumber(): int
    {
        return match ($this) {
            WeekDay::MONDAY => 1,
            WeekDay::TUESDAY => 2,
            WeekDay::WEDNESDAY => 3,
            WeekDay::THURSDAY => 4,
            WeekDay::FRIDAY => 5,
            WeekDay::SATURDAY => 6,
            WeekDay::SUNDAY => 7,
        };
    }
}
