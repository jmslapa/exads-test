<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

class Number
{
    private array $multiples;

    public function __construct(
        private readonly int $value,
    ) {
        $this->multiples = $this->findMultiples();
    }

    protected function findMultiples(): array
    {
        $multiples = [];
        for ($i = $this->value; $i > 0; $i--) {
            if ($this->value % $i === 0) {
                $multiples[] = $i;
            }
        }
        return array_reverse($multiples);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getMultiples(): array
    {
        return $this->multiples;
    }

    public function isPrime(): bool
    {
        return count(
                $this->multiples
            ) === 2 && $this->multiples[0] === 1 && $this->multiples[1] === $this->value;
    }

}