<?php

namespace Jmslapa\ExadsTest\Domain\Entities;

class TvSeries
{
    /**
     * @param int $id
     * @param string $title
     * @param string $channel
     * @param string $gender
     * @param TvSeriesInterval[] $intervals
     */
    public function __construct(
        private int $id,
        private string $title,
        private string $channel,
        private string $gender,
        private array $intervals
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     */
    public function setChannel(string $channel): void
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return TvSeriesInterval[]
     */
    public function getIntervals(): array
    {
        return $this->intervals;
    }

    /**
     * @param TvSeriesInterval[] $intervals
     */
    public function setIntervals(array $intervals): void
    {
        $this->intervals = $intervals;
    }
}
