<?php

namespace Jmslapa\ExadsTest\Domain\Interfaces;

interface ViewModel
{
    public function getView();
}