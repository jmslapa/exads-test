<?php

namespace Jmslapa\ExadsTest\Presentation\Presenters\FindMissingChar;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharResponseModel;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;
use Jmslapa\ExadsTest\Presentation\ViewModels\JsonViewModel;

class FindMissingCharJsonPresenter implements FindMissingCharOutputPort
{
    private static ?FindMissingCharOutputPort $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): FindMissingCharOutputPort
    {
        if (!self::$instance) {
            self::$instance = new FindMissingCharJsonPresenter();
        }

        return self::$instance;
    }

    public function missingCharFoundSuccessfully(
        FindMissingCharResponseModel $responseModel
    ): ViewModel {
        return new JsonViewModel(
            new HttpResponse(
                200,
                ['Content-Type' => 'application/json'],
                json_encode([
                    'data' => $responseModel->getModel()
                ])
            )
        );
    }

}