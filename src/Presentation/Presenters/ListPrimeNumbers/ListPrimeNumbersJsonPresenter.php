<?php

namespace Jmslapa\ExadsTest\Presentation\Presenters\ListPrimeNumbers;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersResponseModel;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;
use Jmslapa\ExadsTest\Presentation\ViewModels\JsonViewModel;

class ListPrimeNumbersJsonPresenter implements ListPrimeNumbersOutputPort
{
    private static ?ListPrimeNumbersOutputPort $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): ListPrimeNumbersOutputPort
    {
        if (!self::$instance) {
            self::$instance = new ListPrimeNumbersJsonPresenter();
        }

        return self::$instance;
    }

    public function numbersListedSuccessfully(
        ListPrimeNumbersResponseModel $responseModel
    ): ViewModel {
        return new JsonViewModel(
            new HttpResponse(
                200,
                ['Content-type' => 'application/json'],
                json_encode([
                    'data' => $responseModel->getModel()
                ])
            )
        );
    }

}