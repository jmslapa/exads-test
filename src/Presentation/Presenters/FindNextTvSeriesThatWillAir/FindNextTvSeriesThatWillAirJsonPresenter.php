<?php

namespace Jmslapa\ExadsTest\Presentation\Presenters\FindNextTvSeriesThatWillAir;

use Jmslapa\ExadsTest\Domain\Entities\TvSeriesInterval;
use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirResponseModel;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;
use Jmslapa\ExadsTest\Presentation\ViewModels\JsonViewModel;

class FindNextTvSeriesThatWillAirJsonPresenter implements
    FindNextTvSeriesThatWillAirOutputPort
{
    private static ?FindNextTvSeriesThatWillAirOutputPort $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): FindNextTvSeriesThatWillAirOutputPort
    {
        if (!self::$instance) {
            self::$instance = new FindNextTvSeriesThatWillAirJsonPresenter();
        }

        return self::$instance;
    }

    public function nextTvSeriesFound(
        FindNextTvSeriesThatWillAirResponseModel $responseModel
    ): ViewModel {
        return new JsonViewModel(
            new HttpResponse(
                status: 200,
                body: json_encode([
                    'data' => [
                        'id' => $responseModel->getTvSeries()->getId(),
                        'title' => $responseModel->getTvSeries()->getTitle(),
                        'channel' => $responseModel->getTvSeries()->getChannel(
                        ),
                        'gender' => $responseModel->getTvSeries()->getGender(),
                        'intervals' => array_map(
                            function (TvSeriesInterval $interval) {
                                return [
                                    'idTvSeries' => $interval->getIdTvSeries(),
                                    'weekDay' => $interval->getWeekDay(),
                                    'showTime' => $interval->getShowTime(),
                                ];
                            },
                            $responseModel->getTvSeries()->getIntervals()
                        )
                    ]
                ])
            )
        );
    }

    public function tvSeriesNotFound(): ViewModel
    {
        return new JsonViewModel(
            new HttpResponse(
                status: 404,
                body: json_encode(['message' => 'Tv series not found.'])
            )
        );
    }

    public function unexpectedError(): ViewModel
    {
        return new JsonViewModel(
            new HttpResponse(
                status: 500,
                body: json_encode(
                    ['message' => 'An unexpected error has occurred. Please, contact the responsible.']
                )
            )
        );
    }

}