<?php

namespace Jmslapa\ExadsTest\Presentation\Presenters\GetRandomPromotionalDesign;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignOutputPort;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignResponseModel;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;
use Jmslapa\ExadsTest\Presentation\ViewModels\JsonViewModel;

class GetRandomPromotionalDesignJsonPresenter implements
    GetRandomPromotionalDesignOutputPort
{
    private static GetRandomPromotionalDesignOutputPort|null $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): GetRandomPromotionalDesignOutputPort
    {
        self::$instance ??= new GetRandomPromotionalDesignJsonPresenter();
        return self::$instance;
    }

    public function promotionalDesignSuccessfullyRetrieved(
        GetRandomPromotionalDesignResponseModel $responseModel
    ): ViewModel {
        $design = $responseModel->getPromotionalDesign();
        $data = [
            'data' => [
                'designId' => $design->getDesignId(),
                'designName' => $design->getDesignName(),
                'splitPercent' => $design->getSplitPercent()
            ]
        ];

        return new JsonViewModel(
            new HttpResponse(
                status: 200,
                body: json_encode($data)
            )
        );
    }

    public function promotionNotFound(): ViewModel
    {
        return new JsonViewModel(
            new HttpResponse(
                status: 404,
                body: json_encode([
                    'message' => 'Promotion not found.'
                ])
            )
        );
    }

    public function promotionalDesignsNotFound(): ViewModel
    {
        return new JsonViewModel(
            new HttpResponse(
                status: 404,
                body: json_encode([
                    'message' => 'Promotional designs not found.'
                ])
            )
        );
    }

    public function unexpectedError(\Throwable $e): ViewModel
    {
        return new JsonViewModel(
            new HttpResponse(
                status: 500,
                body: json_encode(
                    ['message' => 'An unexpected error has occurred. Please, contact the responsible.']
                )
            )
        );
    }

}