<?php

namespace Jmslapa\ExadsTest\Presentation\ViewModels;

use Jmslapa\ExadsTest\Domain\Interfaces\ViewModel;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class JsonViewModel implements ViewModel
{
    public function __construct(
        private readonly HttpResponse $response
    ) {
    }

    public function getView(): HttpResponse
    {
        $this->response->setHeaders([
            ...$this->response->getHeaders(),
            'Content-Type' => 'application/json'
        ]);
        return $this->response;
    }

}