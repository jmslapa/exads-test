<?php

namespace Jmslapa\ExadsTest\Presentation\Gateway\HTTP;

class HttpRequest
{
    private function __construct(
        private string $uri,
        private HttpMethod $method,
        private array $headers,
        private array $queryParameters,
        private array $body,
    ) {
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

    public function getMethod(): HttpMethod
    {
        return $this->method;
    }

    public function setMethod(HttpMethod $method): void
    {
        $this->method = $method;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    public function setQueryParameters(array $queryParameters): void
    {
        $this->queryParameters = $queryParameters;
    }

    public function getBody(): array
    {
        return $this->body;
    }

    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    public static function make(): self
    {
        [$uri] = explode('?', $_SERVER['REQUEST_URI']);
        $method = HttpMethod::from(strtoupper($_SERVER['REQUEST_METHOD']));
        $headers = [];
        foreach ($_SERVER as $key => $value) {
            if (!str_starts_with($key, 'HTTP_')) {
                continue;
            }
            $header = str_replace(
                ' ',
                '-',
                ucwords(str_replace('_', ' ', strtolower(substr($key, 5))))
            );
            $headers[$header] = $value;
        }
        $queryParams = array_reduce(
            explode('&', $_SERVER['QUERY_STRING']),
            function ($result, $expression) {
                if ($expression) {
                    [$key, $value] = explode('=', $expression);
                    $result[$key] = $value;
                }
                return $result;
            },
            []
        );
        $body = str_contains(
            $headers['Content-Type'] ?? '',
            'application/json'
        ) ?
            json_decode(file_get_contents('php://input'), true) :
            $_POST;

        return new HttpRequest(
            $uri,
            $method,
            $headers,
            $queryParams,
            $body ?? []
        );
    }


}