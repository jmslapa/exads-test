<?php

namespace Jmslapa\ExadsTest\Presentation\Controllers\Impl;

use Jmslapa\ExadsTest\Domain\Usecases\ListPrimeNumbers\ListPrimeNumbersInputPort;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class ListPrimeNumbersController implements HttpController
{
    public function __construct(
        private readonly ListPrimeNumbersInputPort $inputPort
    ) {
    }

    public function handle(HttpRequest $request): HttpResponse
    {
        return $this->inputPort->listPrimeNumbers()->getView();
    }
}