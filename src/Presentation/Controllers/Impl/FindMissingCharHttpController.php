<?php

namespace Jmslapa\ExadsTest\Presentation\Controllers\Impl;

use Jmslapa\ExadsTest\Domain\Usecases\FindMissingChar\FindMissingCharInputPort;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class FindMissingCharHttpController implements HttpController
{
    public function __construct(
        private readonly FindMissingCharInputPort $inputPort
    ) {
    }

    public function handle(HttpRequest $request): HttpResponse
    {
        return $this->inputPort->findMissingChar()->getView();
    }
}