<?php

namespace Jmslapa\ExadsTest\Presentation\Controllers\Impl;

use DateTime;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirRequestModel;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class FindNextTvSeriesThatWillAirHttpController implements HttpController
{
    public function __construct(
        private readonly FindNextTvSeriesThatWillAirInputPort $inputPort
    ) {
    }

    public function handle(HttpRequest $request): HttpResponse
    {
        $title = $request->getQueryParameters()['title'] ?? null;
        $dateTime = $request->getQueryParameters()['dateTime'] ?? null;
        if ($dateTime) {
            if (!($dateTime = DateTime::createFromFormat(
                'Y-m-d\TH:i:s',
                $dateTime
            ))) {
                return new HttpResponse(
                    status: 422,
                    headers: ['Content-Type' => 'application/json'],
                    body: json_encode(
                        ['message' => 'The dateTime attribute must be a valid date on format YYYY-MM-DDTHH:MM:SS']
                    )
                );
            }
        }

        return $this->inputPort->findNextTvSeries(
            new FindNextTvSeriesThatWillAirRequestModel(
                title: $title,
                dateTime: $dateTime
            )
        )->getView();
    }

}