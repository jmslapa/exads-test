<?php

namespace Jmslapa\ExadsTest\Presentation\Controllers\Impl;

use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignInputPort;
use Jmslapa\ExadsTest\Domain\Usecases\GetRandomPromotionalDesign\GetRandomPromotionalDesignRequestModel;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class GetRandomPromotionalDesignHttpController implements HttpController
{
    public function __construct(
        private readonly GetRandomPromotionalDesignInputPort $inputPort
    ) {
    }

    public function handle(HttpRequest $request): HttpResponse
    {
        $promotionId = $request->getQueryParameters()['promotionId'] ?? null;
        if (!$promotionId) {
            return new HttpResponse(
                status: 422,
                headers: ['Content-Type' => 'application/json'],
                body: json_encode(
                    ['message' => 'The promotionId parameter is required.']
                )
            );
        }

        $request = new GetRandomPromotionalDesignRequestModel($promotionId);

        return $this->inputPort->getRandomPromotionalDesign($request)
            ->getView();
    }
}