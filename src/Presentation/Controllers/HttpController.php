<?php

namespace Jmslapa\ExadsTest\Presentation\Controllers;

use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

interface HttpController
{
    public function handle(HttpRequest $request): HttpResponse;
}