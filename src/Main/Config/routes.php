<?php

use Jmslapa\ExadsTest\Main\Factories\Controllers\Impl\FindMissingCharControllerFactory;
use Jmslapa\ExadsTest\Main\Factories\Controllers\Impl\FindNextTvSeriesThatWillAirControllerFactory;
use Jmslapa\ExadsTest\Main\Factories\Controllers\Impl\GetRandomPromotionalDesignControllerFactory;
use Jmslapa\ExadsTest\Main\Factories\Controllers\Impl\ListPrimeNumbersControllerFactory;
use Jmslapa\ExadsTest\Main\Routing\HttpRoute;

if (!function_exists('routes')) {
    /**
     * @return HttpRoute[]
     */
    function routes(): array
    {
        return [
            HttpRoute::get(
                '/prime-numbers',
                ListPrimeNumbersControllerFactory::getInstance()->make()
            ),
            HttpRoute::get(
                '/find-missing-char',
                FindMissingCharControllerFactory::getInstance()->make()
            ),
            HttpRoute::get(
                '/find-next-tv-series',
                FindNextTvSeriesThatWillAirControllerFactory::getInstance()
                    ->make()
            ),
            HttpRoute::get(
                '/get-random-promotional-design',
                GetRandomPromotionalDesignControllerFactory::getInstance()
                    ->make()
            )
        ];
    }
}
