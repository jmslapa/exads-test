<?php

try {
    foreach (parse_ini_file(__DIR__ . '/../../../.env') as $key => $value) {
        $_ENV[$key] = $value;
    }
} catch (\Exception $error) {
    // DO NOTHING
} finally {
    if (!function_exists('env')) {
        function env(string $variable, $default = null): ?string
        {
            return $_ENV[$variable] ?? $default;
        }
    }
}
