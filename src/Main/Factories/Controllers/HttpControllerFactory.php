<?php

namespace Jmslapa\ExadsTest\Main\Factories\Controllers;

use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;

interface HttpControllerFactory
{
    public function make(): HttpController;
}