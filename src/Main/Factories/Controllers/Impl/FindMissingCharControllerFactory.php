<?php

namespace Jmslapa\ExadsTest\Main\Factories\Controllers\Impl;

use Jmslapa\ExadsTest\Application\Interactors\FindMissingCharInteractor;
use Jmslapa\ExadsTest\Main\Factories\Controllers\HttpControllerFactory;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Controllers\Impl\FindMissingCharHttpController;
use Jmslapa\ExadsTest\Presentation\Presenters\FindMissingChar\FindMissingCharJsonPresenter;

class FindMissingCharControllerFactory implements HttpControllerFactory
{
    private static ?HttpControllerFactory $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): HttpControllerFactory
    {
        if (!self::$instance) {
            self::$instance = new FindMissingCharControllerFactory();
        }

        return self::$instance;
    }

    public function make(): HttpController
    {
        return new FindMissingCharHttpController(
            new FindMissingCharInteractor(
                FindMissingCharJsonPresenter::getInstance()
            )
        );
    }
}