<?php

namespace Jmslapa\ExadsTest\Main\Factories\Controllers\Impl;

use Jmslapa\ExadsTest\Application\Interactors\FindNextTvSeriesThatWillAirInteractor;
use Jmslapa\ExadsTest\Infra\Database\DatabaseConnectionComposite;
use Jmslapa\ExadsTest\Infra\DataProviders\TvSeriesRepository;
use Jmslapa\ExadsTest\Infra\Helpers\ErrorLoggingHelper;
use Jmslapa\ExadsTest\Infra\Transformers\TvSeriesTransformerImpl;
use Jmslapa\ExadsTest\Main\Factories\Controllers\HttpControllerFactory;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Controllers\Impl\FindNextTvSeriesThatWillAirHttpController;
use Jmslapa\ExadsTest\Presentation\Presenters\FindNextTvSeriesThatWillAir\FindNextTvSeriesThatWillAirJsonPresenter;

class FindNextTvSeriesThatWillAirControllerFactory implements
    HttpControllerFactory
{
    private static ?HttpControllerFactory $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): HttpControllerFactory
    {
        if (!self::$instance) {
            self::$instance = new FindNextTvSeriesThatWillAirControllerFactory(
            );
        }

        return self::$instance;
    }

    public function make(): HttpController
    {
        return new FindNextTvSeriesThatWillAirHttpController(
            new FindNextTvSeriesThatWillAirInteractor(
                FindNextTvSeriesThatWillAirJsonPresenter::getInstance(),
                new TvSeriesRepository(
                    DatabaseConnectionComposite::getInstance()->getConnection(
                        'mysql'
                    ), TvSeriesTransformerImpl::getInstance()
                ),
                ErrorLoggingHelper::getInstance()
            )
        );
    }
}