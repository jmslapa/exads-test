<?php

namespace Jmslapa\ExadsTest\Main\Factories\Controllers\Impl;

use Jmslapa\ExadsTest\Application\Interactors\ListPrimeNumbersInteractor;
use Jmslapa\ExadsTest\Main\Factories\Controllers\HttpControllerFactory;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Controllers\Impl\ListPrimeNumbersController;
use Jmslapa\ExadsTest\Presentation\Presenters\ListPrimeNumbers\ListPrimeNumbersJsonPresenter;

class ListPrimeNumbersControllerFactory implements HttpControllerFactory
{
    private static ?HttpControllerFactory $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): HttpControllerFactory
    {
        if (!self::$instance) {
            self::$instance = new ListPrimeNumbersControllerFactory();
        }

        return self::$instance;
    }

    public function make(): HttpController
    {
        return new ListPrimeNumbersController(
            new ListPrimeNumbersInteractor(
                ListPrimeNumbersJsonPresenter::getInstance()
            )
        );
    }
}