<?php

namespace Jmslapa\ExadsTest\Main\Factories\Controllers\Impl;

use Jmslapa\ExadsTest\Application\Interactors\GetRandomPromotionalDesignInteractor;
use Jmslapa\ExadsTest\Infra\DataProviders\PromotionRepository;
use Jmslapa\ExadsTest\Infra\Helpers\ErrorLoggingHelper;
use Jmslapa\ExadsTest\Infra\Transformers\PromotionalDesignTransformerImpl;
use Jmslapa\ExadsTest\Main\Factories\Controllers\HttpControllerFactory;
use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Controllers\Impl\GetRandomPromotionalDesignHttpController;
use Jmslapa\ExadsTest\Presentation\Presenters\GetRandomPromotionalDesign\GetRandomPromotionalDesignJsonPresenter;

class GetRandomPromotionalDesignControllerFactory implements
    HttpControllerFactory
{
    private static HttpControllerFactory|null $instance = null;

    private function __construct()
    {
    }

    public static function getInstance(): HttpControllerFactory
    {
        self::$instance ??= new GetRandomPromotionalDesignControllerFactory();

        return self::$instance;
    }

    public function make(): HttpController
    {
        return new GetRandomPromotionalDesignHttpController(
            new GetRandomPromotionalDesignInteractor(
                GetRandomPromotionalDesignJsonPresenter::getInstance(),
                new PromotionRepository(
                    PromotionalDesignTransformerImpl::getInstance()
                ),
                ErrorLoggingHelper::getInstance()
            )
        );
    }
}