<?php

namespace Jmslapa\ExadsTest\Main;

use Jmslapa\ExadsTest\Infra\Database\DatabaseConnectionComposite;
use Jmslapa\ExadsTest\Infra\Helpers\ErrorLoggingHelper;
use Jmslapa\ExadsTest\Main\Routing\HttpRouter;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Throwable;

class App
{
    public function run(): void
    {
        $router = new HttpRouter(routes());
        $request = HttpRequest::make();
        $connectionsContainer = DatabaseConnectionComposite::getInstance();
        try {
            $connectionsContainer->open();
            $response = $router->route($request);
            foreach ($response->getHeaders() as $key => $value) {
                header("$key: $value");
            }
            http_response_code($response->getStatus());
            echo $response->getBody();
        } catch (Throwable $e) {
            ErrorLoggingHelper::getInstance()->report('ERROR', $e);
            http_response_code(500);
        } finally {
            $connectionsContainer->close();
        }
    }
}