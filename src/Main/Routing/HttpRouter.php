<?php

namespace Jmslapa\ExadsTest\Main\Routing;

use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpRequest;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpResponse;

class HttpRouter
{
    /**
     * @param HttpRoute[] $routes
     */
    public function __construct(
        private readonly array $routes
    ) {
    }

    public function route(HttpRequest $request): HttpResponse
    {
        $uri = trim($request->getUri(), '/');
        $regex = "/^\/?{$uri}\/?$/";

        foreach ($this->routes as $r) {
            if ($r->getMethod() === $request->getMethod() && preg_match(
                    $regex,
                    $r->getPath()
                )) {
                return $r->getController()->handle($request);
            }
        }

        return new HttpResponse(
            404,
            ['Content-Type' => 'application/json'],
            json_encode(['message' => 'Route not found.'])
        );
    }
}