<?php

namespace Jmslapa\ExadsTest\Main\Routing;

use Jmslapa\ExadsTest\Presentation\Controllers\HttpController;
use Jmslapa\ExadsTest\Presentation\Gateway\HTTP\HttpMethod;

class HttpRoute
{
    private function __construct(
        private readonly HttpMethod $method,
        private readonly string $path,
        private readonly HttpController $controller
    ) {
    }

    /**
     * @return HttpMethod
     */
    public function getMethod(): HttpMethod
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return HttpController
     */
    public function getController(): HttpController
    {
        return $this->controller;
    }

    public static function get(
        string $path,
        HttpController $controller
    ): HttpRoute {
        return new HttpRoute(HttpMethod::GET, $path, $controller);
    }

    public static function post(
        string $path,
        HttpController $controller
    ): HttpRoute {
        return new HttpRoute(HttpMethod::POST, $path, $controller);
    }

    public static function put(
        string $path,
        HttpController $controller
    ): HttpRoute {
        return new HttpRoute(HttpMethod::PUT, $path, $controller);
    }

    public static function patch(
        string $path,
        HttpController $controller
    ): HttpRoute {
        return new HttpRoute(HttpMethod::PATCH, $path, $controller);
    }

    public static function delete(
        string $path,
        HttpController $controller
    ): HttpRoute {
        return new HttpRoute(HttpMethod::DELETE, $path, $controller);
    }

}