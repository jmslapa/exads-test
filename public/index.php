<?php
require __DIR__.'/../vendor/autoload.php';

use Jmslapa\ExadsTest\Main\App;

$app = new App();

$app->run();