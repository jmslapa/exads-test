<?php


use Phinx\Seed\AbstractSeed;

class TvSeriesIntervalsSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'TvSeriesSeeder'
        ];
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $this->execute("
            insert into tv_series_intervals (id_tv_series, week_day, show_time)
            values (1, 'SUNDAY', '21:00'),
                   (2, 'SUNDAY', '09:30'),
                   (3, 'THURSDAY', '03:00')
        ");
    }
}
