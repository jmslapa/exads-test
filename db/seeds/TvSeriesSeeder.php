<?php


use Phinx\Seed\AbstractSeed;

class TvSeriesSeeder extends AbstractSeed
{
    public function run()
    {
        $this->execute("
            insert into tv_series (title, channel, gender)
            values ('Breaking Bad', 'AMC', 'Crime drama'), 
                   ('One Piece', 'Fuji TV', 'Anime'), 
                   ('Halo', 'Paramount Plus', 'Sci-fy'); 
        ");
    }
}
