<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTvSeriesIntervalsTable extends AbstractMigration
{
    public function up()
    {
        $this->execute("
            create table tv_series_intervals(
                id_tv_series int not null,
                week_day enum('SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'SATURDAY') not null,
                show_time time not null,
                foreign key (id_tv_series) references tv_series(id),
                primary key (id_tv_series, week_day, show_time)
            );
        ");
    }

    public function down()
    {
        $this->execute("
            drop table tv_series_intervals;
        ");
    }
}
