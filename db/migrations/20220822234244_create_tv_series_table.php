<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTvSeriesTable extends AbstractMigration
{
    public function up() {
        $this->execute("
            create table tv_series (
                id int not null primary key auto_increment,
                title varchar(100) not null,
                channel varchar(60) not null,
                gender varchar(60) not null
            );
        ");
    }

    public function down() {
        $this->execute("
           drop table tv_series; 
        ");
    }
}
